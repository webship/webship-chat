# Webship Chat

A simple Meteor chat client development test for new applicants.

## Meteor

- Installation: https://www.meteor.com/install
- Documentation: https://docs.meteor.com/#/full/

## Assessments

This test should give us a good idea of an applicants skill level in the following key areas:

- Git management
- Test-driven development
- front- and back-end development
- The Meteor framework
- Meteor reactivity
- MongoDB management

## Brief

Send us a pull request which turns this empty repository into a minimal yet functional chat application with the following properties:

- There is one big receiver in the chat application (@receiver) and two other users (@userone and @usertwo)
- All users should be able to send messages to @receiver
- @receiver should be able to send messages to @userone and @usertwo seperately without the other being able to see said messages
- The users should not be able to send eachother messages. Even if they know eachothers handle (e.g.: @userone)
- The login system can just be an input that sets a session variable. One simply needs to enter "userone" and hit a button to login as userone.

## Requirements

- All code should be written from scratch
- Don't use any layout frameworks like bootstrap
- CSS should be written in either LESS (or pure CSS if unfamiliar with LESS)
- Markup should be valid HTML with Blaze as the logic framework
- Write tests for all server side methods and publications
- Use logical commits with good commit messages
- The result should be handed in through a pull request on this repository, or a link to a public fork

## Briefing and questions

- We will have a call with you to discuss the brief of the project and help you get started.
- You will have a chance to ask any additional questions at two additional calls.

## Deadline

- The project will need to be handed once 4 hours have passed.
- Make sure you have at least thought about how you would handle every aspect of the application and write it out in pseudo code in case you are short on time.
- In case you have time to spare however, consider implementing useful features on top of the base application. Cool ideas are always encouraged.
